//
// Created by Rulin Tang on 15/12/18.
//

#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main() {
    int sockfd;
    char buffer[1024] = "Hello World";
    struct sockaddr_in server_addr;
    //struct hostent *host;
    in_port_t port = 4444;
    int nbytes;
    char host[] = "127.0.0.1";


    if((sockfd=socket(AF_INET,SOCK_STREAM,0))==-1)
    {
        fprintf(stderr,"Socket Error:%s\a\n",strerror(errno));
        exit(1);
    }

    bzero(&server_addr, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port);
    inet_aton(host,&server_addr.sin_addr);

    printf("%d\n",(int)strlen(buffer));

    /* 客户程序发起连接请求         */
    if (connect(sockfd, (struct sockaddr *) (&server_addr), sizeof(struct sockaddr)) == -1) {
        fprintf(stderr, "Connect Error:%s\a\n", strerror(errno));
        exit(1);
    }

    if ((nbytes = write(sockfd, buffer, 1024)) == -1) {
        fprintf(stderr, "Write Error:%s\n", strerror(errno));
        exit(1);
    }

    if ((nbytes = read(sockfd, buffer, 1024)) == -1) {
        fprintf(stderr, "Read Error:%s\n", strerror(errno));
        exit(1);
    }
    printf("%d\n",(int)strlen(buffer)+1);
    printf("%s\n", buffer);
}

